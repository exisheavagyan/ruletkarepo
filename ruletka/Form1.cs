﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ruletka
{
    public partial class Form1 : Form
    {
        Random rand;
        int count = 0;
        int total = 0;
        enum Status
        {
            win = 1,
            lost = 2
        }
        private void ShowMessage(Status status)
        {
             switch (status)
             {
                case Status.win:
                    {
                        DialogResult dr = MessageBox.Show("You Win " + this.total + "Do you wont to try again", "Result", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if(dr == DialogResult.Yes)
                        {
                            this.ResetForm();
                        }
                        else
                        {
                            this.Close();
                        }
                        break;

                    }
                    
                   
                case Status.lost:
                    {
                        DialogResult dr = MessageBox.Show("You Lost. Do you wont to try again", "Result", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            this.ResetForm();
                        }
                        else
                        {
                            this.Close();
                        }
                        break;
                    }

                    
            }

        }
        public Form1()
        {
            InitializeComponent();
            this.totalValueLbl.Text = this.total.ToString();
            rand =  new Random();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void x_Click(object sender, EventArgs e)
        {
            if (this.bet.Text=="Bet")
            {
                MessageBox.Show("Please choose a bet value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if(this.count < 5)
            {
                this.l1.Text = getRandomValue().ToString();
                this.l2.Text = getRandomValue().ToString();
                this.l3.Text = getRandomValue().ToString();
                this.count++;
                this.CalculateTotal();
            }
            else
            {
                if (this.total == 0)
                {
                    ShowMessage(Status.lost);
                }
                else
                {
                    ShowMessage(Status.win);
                }
            }
        }

        byte getRandomValue()
        {
           return (byte) rand.Next(1, 9);
        }
        void CalculateTotal()
        {
            if (this.l1.Text == this.l2.Text && this.l1.Text == this.l3.Text)
            {
                this.total += Convert.ToInt32(this.bet.Text) * Convert.ToInt32(this.l1.Text);
            }
           else if(this.l1.Text == this.l2.Text || this.l1.Text == this.l3.Text) 
            {
                this.total +=  (int)Math.Round( Convert.ToDouble(this.bet.Text) * Convert.ToDouble(this.l1.Text) / 2);
            }
            else if (this.l2.Text == this.l3.Text)
            {
                this.total += (int)Math.Round(Convert.ToDouble(this.bet.Text) * Convert.ToDouble(this.l2.Text) / 2);
            }
            else if (this.l3.Text == (Convert.ToInt32(this.l2.Text)+1).ToString() && this.l3.Text == (Convert.ToInt32(this.l1.Text) + 2).ToString())
            {
                this.total += Convert.ToInt32(this.l2) * 2;
            }
            else if (this.l1.Text == (Convert.ToInt32(this.l2.Text) - 1).ToString() && this.l1.Text == (Convert.ToInt32(this.l3.Text) - 2).ToString())
            {
                this.total += Convert.ToInt32(this.l2) * 2;
            }
            this.totalValueLbl.Text = this.total.ToString();
        }

        private void ResetForm() 
        {
            this.total = 0;
            this.count = 0;
            this.l1.Text = "0";
            this.l2.Text = "0";
            this.l3.Text = "0";
            this.totalValueLbl.Text = this.total.ToString();
            this.bet.Text = "Bet";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
